public class BadPracticesExample {

    // Hardcoded credentials (Security Hotspot)
    private String username = 'admin';
    private String password = 'password123';

    // Method with too many lines of code (Code Smell)
    public void processAccounts(List<Account> accounts) {
        // DML statements inside a loop (Bug)
        for (Account acc : accounts) {
            acc.Name = 'Updated Name';
            update acc; // DML inside a loop
        }

        // Useless conditional (Bug)
        if (true) {
            System.debug('This is unnecessary'); // Duplicate string literal (Code Smell)
        }

        // Hardcoded record ID (Bug)
        String accountId = '001xx000003DGb2AAG';
        delete [SELECT Id FROM Account WHERE Id = :accountId];

        // Identical if/else branches (Code Smell)
        if (username == 'admin') {
            doAdminTask();
        } else if (username == 'admin') {
            doAdminTask();
        }

        // Non-existent operator (Bug)
        Integer count = 5;
        count =+ 10; // Should be 'count += 10;'
    }

    // Unused method parameter (Code Smell)
    private void doAdminTask() {
        // Some admin task logic here
    }

    // Hardcoded IP address (Security Hotspot)
    public void connectToExternalService() {
        String serviceUrl = 'http://192.168.1.1/service'; // Using hardcoded IP address
        // Logic to connect to an external service
    }

    // Nested if statements (Code Smell)
    public void checkData(String data) {
        if (data != null) {
            if (data.length() > 0) {
                // Some logic
            }
        }
    }
}
